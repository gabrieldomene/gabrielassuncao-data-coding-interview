
import os
import sys
import argparse
import logging
import psycopg2
import psycopg2.extras
import json

def _get_config(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', dest='host', type=str)
    parser.add_argument('--port', dest='port', type=int)
    parser.add_argument('--username', dest='username', type=str)
    parser.add_argument('--password', dest='password', type=str)
    parser.add_argument('--database', dest='database', type=str)
    parser.add_argument('--table', dest='table', type=str)
    parser.add_argument('--source', dest='source', type=str)
    parser.add_argument('--delimiter', dest='delimiter', type=str, default=',')
    config, _ = parser.parse_known_args(argv)

    return config


def _connect_db(config):
    """Create database connection

    :param config Configuration parameters
    """
    
    conn = psycopg2.connect(host=config.host, user=config.username,
                            port=config.port, password=config.password,
                            dbname=config.database)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    return conn, cursor


def _get_source_path(source):
    """Build absolute source file path

    :param source Source path
    """
    current_path = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(current_path, source)

def _get_prepared_row(headers, row):
  h = headers.replace('\n', '').split(',')
  r = row.replace('\n', '').split(',')
  return dict(zip(h, r))

def main(argv):
    try:
        config = _get_config(argv)
        conn, cursor = _connect_db(config)
        source_path = _get_source_path(config.source)

        try:
            # Here was a listdir option to fetch all files from the dir
            files = os.listdir(source_path)
            for file in files:
                # open 1 csv for the sake of the demo test
                with open(source_path + '/nyc_airlines.csv', 'r') as f:
                    raw_header = f.readline()
                    for line in f:
                        raw_row = line
                        content = _get_prepared_row(raw_header, raw_row)
                        cols = ', '.join(content.keys())
                        values = ', '.join(['%s'] * len(content.values()))
                        query = f"INSERT INTO airlines ({cols}) VALUES ({values})"
                        cursor.execute(query, list(content.values()))
                        # cursor.execute("INSERT INTO airlines %s VALUES %s", [content.keys(), content.values()])
                conn.commit()
        finally:
            cursor.close()
            conn.close()
    except Exception as e:
        logging.exception(e)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    main(sys.argv)
